package com.example.appcuestionario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
private RadioGroup rgrupo1, rgrupo2, rgrupo3, rgrupo4, rgrupo5, rgrupo6, rgrupo7, rgrupo8, rgrupo9, rgrupo10, rgrupo11;
private TextView resultado;
private Button calcular;
private CheckBox check1,check2,check3;
private Switch sw1,sw2,sw3;
private ToggleButton tog1, tog2,tog3;
private int res=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rgrupo1 = findViewById(R.id.rgrup1);
        rgrupo2 = findViewById(R.id.rgrup2);
        rgrupo3 = findViewById(R.id.rgrup3);
        rgrupo4 = findViewById(R.id.rgrup4);
        rgrupo5 = findViewById(R.id.rgrup5);
        rgrupo6 = findViewById(R.id.rgrup6);
        rgrupo7 = findViewById(R.id.rgrup7);
        rgrupo8 = findViewById(R.id.rgrup8);
        rgrupo9 = findViewById(R.id.rgrup9);
        rgrupo10 = findViewById(R.id.rgrup10);
        rgrupo11 = findViewById(R.id.rgrup11);


        check1 = findViewById(R.id.check1p12);
        check2 = findViewById(R.id.check3p13);
        check3 = findViewById(R.id.check2p14);


        sw1 = findViewById(R.id.sw15);
        sw2 = findViewById(R.id.sw16);
        sw3 = findViewById(R.id.sw17);


        tog1 = findViewById(R.id.tg1p18);
        tog2 = findViewById(R.id.tg2p19);
        tog3 = findViewById(R.id.tg3p20);


        resultado=findViewById(R.id.resultadoview);

        calcular=findViewById(R.id.calcular);


    }

    public void calcularRes(View view) {
        res=0;
        revisarRespuestas();

        resultado.setText(String.valueOf(res+"/20"));

    }
    private void revisarRespuestas(){

        res= rgrupo1.getCheckedRadioButtonId()== R.id.resp3p1 ? res+1:res;
        res= rgrupo2.getCheckedRadioButtonId()== R.id.resp1p2 ? res+1:res;
        res= rgrupo3.getCheckedRadioButtonId()== R.id.resp3p3 ? res+1:res;
        res= rgrupo4.getCheckedRadioButtonId()== R.id.resp1p4 ? res+1:res;
        res= rgrupo5.getCheckedRadioButtonId()== R.id.resp2p5 ? res+1:res;
        res= rgrupo6.getCheckedRadioButtonId()== R.id.resp2p6 ? res+1:res;
        res= rgrupo7.getCheckedRadioButtonId()== R.id.resp1p7 ? res+1:res;
        res= rgrupo8.getCheckedRadioButtonId()== R.id.resp3p8 ? res+1:res;
        res= rgrupo9.getCheckedRadioButtonId()== R.id.resp2p9 ? res+1:res;
        res= rgrupo10.getCheckedRadioButtonId()== R.id.resp1p10 ? res+1:res;
        res= rgrupo11.getCheckedRadioButtonId()== R.id.resp3p11 ? res+1:res;


        res= check1.isChecked()? res+1:res;
        res= check2.isChecked()? res+1:res;
        res= check3.isChecked()? res+1:res;


        res= sw1.isChecked()? res:res+1;
        res= sw2.isChecked()? res+1:res;
        res= sw3.isChecked()? res+1:res;


        res= tog1.isChecked()? res +1:res;
        res= tog2.isChecked()? res +1:res;
        res= tog3.isChecked()? res :res+1;

    }
}
